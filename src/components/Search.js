import React, { Component } from 'react';

class Search extends Component {
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input
          id="search"
          value=""
          placeholder="Search..."
        />
        <button>Search</button>
      </form>
    );
  }
}

export default Search;

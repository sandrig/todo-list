import React, { Component } from 'react';

class Footer extends Component {
  render() {
    return (
      <div className="copyright">&copy; TODO List {new Date().getFullYear()}</div>
    );
  }
}

export default Footer;

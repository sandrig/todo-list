import React, { Component } from 'react';

class List extends Component {
  render() {
    return (
      <ul>
        {this.props.items.map(item => (
          <li key={item.id}>
            <div>Date: {item.date.toLocaleString()}</div>
            <div>Text: {item.text}</div>
            <div>Author: Andrey Savelev</div>
            <button>Edit</button>
            <button onClick={(e) => this.props.handleDelete(item.id, e)}>Delete</button>
          </li>
        ))}
      </ul>
    );
  }
}

export default List;

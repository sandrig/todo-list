import React, { Component } from 'react';
import Footer from './Footer';
import List from './List';
import Login from './Login';
import Search from './Search';


class TodoApp extends Component {
  state = {
    items: [],
    text: ''
  };

  handleDelete = (id) => {
    const newItems = this.state.items.filter((item) => item.id !== id);
    this.setState({ items: newItems });
  }

  handleChange = (e) => {
    this.setState({ text: e.target.value });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    if (!this.state.text.length) {
      return;
    }

    const newItem = {
      text: this.state.text,
      id: Date.now(),
      date: new Date(),
    };

    this.setState(prevState => ({
      items: prevState.items.concat(newItem).reverse(),
      text: ''
    }));
  }

  render() {
    return (
      <div className="wrapper">
        <div className="todo">
          <div className="todo__login">
            <Login />
          </div>
          <div className="todo__body">
            <h1>TODO List</h1>
            <div className="todo__search">
              <Search />
            </div>
            <div className="todo__form">
              <form onSubmit={this.handleSubmit}>
                <input
                  onChange={this.handleChange}
                  value={this.state.text}
                />
                <button>Add</button>
              </form>
            </div>
            <List
              handleDelete={this.handleDelete}
              items={this.state.items} />
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default TodoApp;
